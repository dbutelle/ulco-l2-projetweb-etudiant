<?php


namespace controller;


class CartController
{
    public static function cart():void{

        $params = [
            "title"  => "Panier",
            "module" => "cart.php"
        ];

        \view\Template::render($params);
    }

    public static function addToCart(): void
    {
    if(isset($_SESSION['id'])){
        if ($_POST['chiffre'] <= 0)
            unset($_SESSION['cart'][$_POST['name']]);
        else{
            $_SESSION['cart'][$_POST['name']]['id'] = $_POST['name'];
            $_SESSION['cart'][$_POST['name']]['chiffre'] = $_POST['chiffre'];
        }

        $params = array(
            "title" => "Mon panier",
            "module" => "cart.php",
        );

        \view\Template::render($params);
    }else{
        header('Location:/store');
    }

    }


}
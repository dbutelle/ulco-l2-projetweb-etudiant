<?php

namespace controller;

class StoreController {

    public static function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $products = \model\StoreModel::listProducts();
    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
        "products"=>$products,
        "valid"=>$_GET['status']??null
    );

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }
  public static function product(int $id,string $valid="null"):void{

      $infos=\model\StoreModel::infoProduct($id);
      $products= \model\StoreModel::listProducts();
      $comments = \model\CommentModel::listComment($id);
        foreach ($products as $produits){
            if($id > count($produits) || $id==0){
                header('Location:/store');
            }
        }
      foreach ($infos as $info){
            $name=$info['name'];
        }
            $params = array(
                "title" => $name,
                "module" => "product.php",
                "infos" => $infos,
                "comments" => $comments,
                "valid"=>$valid
            );

      \view\Template::render($params);
  }
    public static function search():void{
        $categories = \model\StoreModel::listCategories();
        $products = \model\StoreModel::listProducts($_POST['search']??'',$_POST['category']??null,$_POST['order']??'none',);
        $params = array(
            "title" => "Store",
            "module" => "store.php",
            "categories" => $categories,
            "products"=>$products,
            "valid"=>($_GET['status']??null)
        );
        \view\Template::render($params);
    }
}
<?php

namespace model;

class StoreModel
{

    static function listCategories(): array
    {
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT id, name FROM category";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }

    static function listProducts(string $filterName = "", array $categories = null, string $tri = "none",): array
    {
        $db = \model\Model::connect();
        $filter = "";
        $triSQL = "";
        if ($categories != null) {
            for ($i = 0; $i < count($categories); $i++) {
                $categories[$i] = "'" . $categories[$i] . "'";
            }
        }
        if ($filterName != "") {
            $filter = " WHERE product.name LIKE '%$filterName%' ";
        }
        if ($tri === "asc") {
            $triSQL = " ORDER BY product.price ASC ";
        } else if ($tri === "desc") {
            $triSQL = " ORDER BY product.price DESC ";
        }

        if ($categories != null) {
            if ($filter === "") {
                $filter = " WHERE category.name IN (" . implode(',', $categories) . ") ";
            } else {
                $filter .= " AND category.name IN (" . implode(',', $categories) . ") ";
            }
        }
        $sql = "SELECT product.id,product.name,price,image,category.name as category FROM product INNER JOIN category ON product.category=category.id " . $filter . $triSQL;

        $req = $db->prepare($sql);
        $req->execute();

        return $req->fetchAll();
    }

    static function infoProduct(int $id): array
    {
        $db = \model\Model::connect();
        $sql = "SELECT product.id as id,product.name,price,image,image_alt1,image_alt2,image_alt3,spec,category.name as category FROM product INNER JOIN category ON product.category=category.id WHERE
        product.id = '$id'";

        $req = $db->prepare($sql);
        $req->execute();

        return $req->fetchAll();
    }
}
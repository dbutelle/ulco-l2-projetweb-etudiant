<div id="store">

<!-- Filtrer l'affichage des produits  ---------------------------------------->

<form method="post" action="/store/search">

  <h4>Rechercher</h4>
  <input type="text" name="search" placeholder="Rechercher un produit" />

  <h4>Catégorie</h4>
  <?php foreach ($params["categories"] as $c) { ?>
    <input type="checkbox" name="category[]" value="<?= $c["name"] ?>" />
    <?= $c["name"] ?>
    <br/>
  <?php } ?>

  <h4>Prix</h4>
  <input type="radio" name="order" value="asc"/> Croissant <br />
  <input type="radio" name="order" value="desc"/> Décroissant <br />

  <div><input style="color:white" type="submit" value="Appliquer" /></div>

</form>

<!-- Affichage des produits --------------------------------------------------->

<div  style="background-color: white;" class="products">
    <?php foreach ($params['products'] as $p){ ?>
    <div class="card">
        <p class="card-image">
            <img src="/public/images/<?php echo $p['image'];?>"
                 </p>
        <p class="card-category">
            <?php echo $p['category'];?>
        </p>
        <p style="color:#253c59" class="card-title">
            <a href="/store/<?php echo $p['id'];?>">
                <?php echo $p['name'];?>
            </a>
        </p>
        <p style="color:#253c59" class="card-price">
            <?php echo $p['price'];?>€
        </p>
    </div>
    <?php } ?>


</div>

</div>

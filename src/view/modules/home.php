<div id="home">

    <div class="banner">
        <h1>L'espace est infini, votre créativité aussi</h1>
        <h2>Parce que vous seul pouvez décider de vos limites</h2>
        <p><a  style="color:#122640" href="/store">Visiter la boutique</a></p>
    </div>

    <div class="brief">
        <div class="brief-image">
            <img width="300" src="/public/images/home_1.png"/>
        </div>
        <div class="brief-text">
            <h3>Notre histoire</h3>
            <p>SpaceView est une entreprise familiale faite par des passionnés
                pour des passionés. Une gamme de téléscope aussi vaste que notre
                domaine de prédilection qu'est l'espace. Parce qu'il ne reste qu'une
                chose à faire pour nous rejoindre, c'est de visiter notre boutique
                pour acquérir l'un de nos bijoux.
                Qu'attendez-vous pour nous rejoindre ?
            </p>
            <p>
                <a href="/store">Nous rejoindre</a>
            </p>
        </div>
    </div>

    <div class="brief">

        <div class="brief-text">
            <h3>Nous suivre</h3>
            <p>Passionnés et passionées nous vous attendons sur nos réseaux
                pour nous montrer vos créations photographiques avec vos
                téléscopes. Nous aussi on veut vous découvrir.
            </p>
            <p>
                <a href="https://fr-fr.facebook.com//">Facebook</a>
                <a href="https://twitter.com/">Twitter</a>
                <a href="https://www.youtube.com">YouTube</a>
            </p>
        </div>
        <div class="brief-image">
            <img width="600" src="/public/images/home_2.png"/>
        </div>
    </div>










</div>
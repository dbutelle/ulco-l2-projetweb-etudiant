<?php
    if(isset($params['valid']) && $params['valid']=="changed"){
        ?><div class="box" id="valid">
        <h3>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check2-circle" viewBox="0 0 16 16">
                <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
            </svg>
        Informations modifiées avec succés.
        </h3>
    </div>
        <?php
    }elseif(isset($params['valid']) && $params['valid'] == "not_changed"){
    ?>
        <div class="box" id="invalid">
        <h3>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
            </svg>
            Vos informations n'ont pas pu être changées.
        </h3>
    </div>
        <?php
    }
?>

<div id="infos">
    <form class="modifs" method="post" action="update">
        <h2 style="padding-bottom: 25px;">Informations du compte</h2>
        <h4>Informations personnelles</h4>
        <p>Prénom</p>
        <input type="text" name="infosprenom" placeholder="Prénom" value="<?php echo $_SESSION['prenom'];?>"/>

        <p>Nom</p>
        <input type="text" name="infosnom" placeholder="Nom" value="<?php echo $_SESSION['nom'];?>"/>

        <p>Adresse mail</p>
        <input type="text" name="infosmail" placeholder="Adresse mail" value="<?php echo $_SESSION['mail'];?>"/>

        <input type="submit" style="color:white;" value="Modifier mes informations" />
    </form>
    <div class="commandes">
        <h2>
            Commandes
        </h2>
        <p>
            Tu n'as pas de commande en cours.
        </p>
    </div>
</div>
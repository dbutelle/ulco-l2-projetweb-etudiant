<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="/public/styles/default.css" />

<div id="error">
    <h1>
        Oups
    </h1>
    <p>On dirait que la page demandée n'existe pas ! </p>
    <a href="/">Retour à l'accueil</a>
</div>